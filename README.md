# README #

### What is this repository for? ###

This is Finnish translations I made from scratch for multi-platform notetaking application CherryTree. Made with Poedit using GetText Portable Object -file. 

[cherrytree – giuspen](https://www.giuspen.com/cherrytree/)
[GitHub - giuspen/cherrytree: cherrytree](https://github.com/giuspen/cherrytree)
